@echo off
set /p sqlitepath=<sqlite.path
set databasepath=%~1
@echo %databasepath%
%sqlitepath% %databasepath% "create table money(timestamp datetime default current_timestamp, note text, sum decimal(15,2), guid char(36) default (lower(hex(randomblob(4)))||'-'||lower(hex(randomblob(2)))||'-4'||substr(lower(hex(randomblob(2))),2)||'-'||substr('ab89', 1 + (abs(random()) %% 4), 1)||substr(lower(hex(randomblob(2))),2)||'-'||lower(hex(randomblob(6)))));"
@echo %databasepath% > database.path