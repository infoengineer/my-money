@echo off
rem chcp 1251 > nul
set /p sqlitepath=<sqlite.path
set /p databasepath=<database.path
set csvpath=%~1
%sqlitepath% %databasepath% ".mode csv" ".import %csvpath% money"